package com.lsgp.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ExpandableArrayTest.class, maximumNumberTest.class, minimumNumberTest.class })
public class AllTests {

}
