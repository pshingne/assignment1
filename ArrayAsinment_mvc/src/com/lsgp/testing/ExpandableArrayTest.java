package com.lsgp.testing;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ls.MyArrayList.MyArrayList;

public class ExpandableArrayTest {

	@Test
	public void test() {
		int a[]={13,10,45,36,47,78,90,57,76,99};
		MyArrayList arr=new MyArrayList();
		int count=0;
		for (int i = 0; i < a.length; i++) {
			arr.addData(a[i]);
			count++;
			
		}
		assertEquals(count, a.length);
	}

}
