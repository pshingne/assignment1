package com.lsgp.ArrayController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.ls.MyArrayList.MyArrayList;
import com.lsgp.ArrayView.ArrayView;
//Array controller
public class ArrayController {
	private ArrayView theView;
	private MyArrayList theModel;

	 public ArrayController(ArrayView theView2, MyArrayList theModel2) {
		
		 
		 this.theView=theView2;
			this.theModel=theModel2;
			
			this.theView.addCalculationListener(new CalculateListener());  //add array elements
			this.theView.minimumNumberListener(new findMinListener());     //find minimum
			
			this.theView.maximumNumberlistener(new findMaxlistener()); //find maximum
			this.theView.clearArrayListener(new clrArrayListener());
			
	}
	 class clrArrayListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				
			       if (theModel.isEmpty())
			    	   theView.displayErrorMessage("Please enter array elements...");
			       else{
					theView.clrArrayElements();
					theModel.clearArray();
					theModel.display();
			       }
					
					
				
				
				
			}
			 
			 
		 }
	 
	 
	 
	 class findMinListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			try {
				theView.setMin(theModel.findMin());
			} catch (Exception e2) {
				theView.displayErrorMessage("Please enter array elements...");
				
			}
			
			
		}
		 
		 
	 }
	 
	 class findMaxlistener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				
				try{
				theView.setMax(theModel.findMax());
				}catch (Exception ex) {
					theView.displayErrorMessage("Please enter array elements...");
				}
				
				
			}
			 
			 
		 }
		 
	 
	class CalculateListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			int Number , i;
			try {
				Number=theView.getNumber();
				theModel.addData(Number);
				
				
				//System.out.println(Number);
				//System.out.println(theModel.display());
				
				theView.setArrayElements(theModel.display());
				theView.clrText();
				
				
				
				
				
			} catch (NumberFormatException ex) {
				theView.displayErrorMessage("you need to add Integers");
				
			}
			
			
		}
		
	}

}
