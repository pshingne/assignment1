package com.lsgp.ArrayView;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ArrayView extends JFrame{
	private JLabel addnumber=new JLabel("Add Number in Array");
	private JTextField Number=new JTextField(5);
	
	private JButton addButton=new JButton("+");
	private JButton clrArray=new JButton("Clear Array");
	private JButton min=new JButton("Find Minimun");
	private JButton max=new JButton("Find Maximum");
	private JTextField arrayElement=new JTextField(50);
	private JTextField minNumber=new JTextField(10);
	private JTextField maxNumber=new JTextField(10);
	
	 public ArrayView()
	{
		JPanel arrayPanel=new JPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600,600);
		arrayPanel.add(addnumber);
		arrayPanel.add(Number);
		arrayPanel.add(addButton);
		arrayPanel.add(arrayElement);
		arrayPanel.add(min);
		arrayPanel.add(minNumber);
		arrayPanel.add(max);
		
		
		arrayPanel.add(maxNumber);
		arrayPanel.add(clrArray);
		this.add(arrayPanel);
	}
	
	
	public int getNumber()
	{
		return Integer.parseInt(Number.getText());
	}
	
	
	public void setArrayElements(StringBuffer n)  // Dislay the array elements in (arrayelement )text Field
	{
		arrayElement.setText(n.toString());
		
		
	}
	public void clrArrayElements()  // Dislay the array elements in (arrayelement )text Field
	{
		arrayElement.setText("");
		minNumber.setText("");
		maxNumber.setText("");
		
		
	}
	public void setMin(int min)   // set minimum number in text fileld 
	{
		minNumber.setText(Integer.toString(min));
	}
	public void setMax(int max)  // set maximum number in text fileld 
	{
		maxNumber.setText(Integer.toString(max));
	}
	public void clrText() //after clicking the '+' button this method clr the text filed
	{
		Number.setText("");
	}
	public void addCalculationListener(ActionListener listenerForCalcButton)
	{
		addButton.addActionListener(listenerForCalcButton);
		
		
		
	}
	
	public void clearArrayListener(ActionListener listenerForCalcButton)
	{
		clrArray.addActionListener(listenerForCalcButton);
		
		
		
	}
	public void minimumNumberListener(ActionListener listenerForCalcButton)
	{
		min.addActionListener(listenerForCalcButton);
		
	}
	public void maximumNumberlistener(ActionListener listenerForCalcButton)
	{
		max.addActionListener(listenerForCalcButton);
		
	}
	
	public void displayErrorMessage(String errorMessage) // To show exception massages
	{
		JOptionPane.showMessageDialog(this, errorMessage);
	}


	
	

}
