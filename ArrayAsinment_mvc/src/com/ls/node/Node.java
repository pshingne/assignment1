package com.ls.node;
/*This class is for to create node of Array list*/
public class Node {
	private int data; //integer element
	private Node link; //link to the next node

	public Node() {

		this.data = 0;
		this.link = null;

	}

	public Node(int data, Node link) {
		this.data = data;
		this.link = link;
	}

	public int getData() {
		return this.data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public void setLink(Node n) {
		this.link = n;
	}

	public Node getLink() {
		return link;
	}

}
