package com.ls.MyArrayList;

import com.ls.node.Node;

public class MyArrayList {
	private Node head;
	private Node tail;

	public MyArrayList() {
		this.head = null;
		this.tail = null;

	}

	public boolean isEmpty() {
		return head == null;
	}

	public void addAtfirst(int data) {
		Node newNode = new Node(data,
				null); /*
						 * if array is empty then this method is called to add
						 * element at starting position
						 */
		if (this.head == null) {
			head = newNode;
			tail = newNode;
		} else {
			newNode.setLink(head);
			head = newNode;
		}

	}

	/* this method is used to add the new element in array */
	/* by default data is add at last position */
	public void addData(int data) {
		Node newNode = new Node(data, null);
		if (head == null) {
			head = newNode;
			newNode.setLink(null);
			tail = newNode;

		} else {
			tail.setLink(newNode);
			tail = newNode;
		}
	}

	/* this method is used to display the array elements */
	public StringBuffer display() {
		StringBuffer str = new StringBuffer();
		Node trav = this.head;
		if (trav == null)
			System.out.println("Array is empty.");
		while (trav != null) {

			str.append("  " + trav.getData());
			// System.out.println(trav.getData());
			trav = trav.getLink();
		}
		System.out.println("the elements are:" + str);
		return str;
	}

	public Node getlast() {
		return tail;
	}

	// for finding minimum and maximum element of array
	public int findMax() {
		Node trav = this.head;
		int max = this.head.getData();
		while (trav != null) {
			if (max < trav.getData())
				max = trav.getData();
			trav = trav.getLink();
		}
		System.out.println("Maximum Number is: " + max);
		return max;
	}

	public int findMin() {
		Node trav = this.head;
		int min = this.head.getData();
		while (trav != null) {

			if (trav.getData() < min)
				min = trav.getData();
			trav = trav.getLink();
		}
		System.out.println("Minimum Number is: " + min);
		return min;
	}
	public void clearArray() {
		
		this.head=this.tail=null;
	
	}

}
