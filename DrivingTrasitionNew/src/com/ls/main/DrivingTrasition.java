package com.ls.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.ls.model.Constants;
import com.ls.model.DrivingSession;

public class DrivingTrasition {

	public void readAndWriteCsv(String srcfile, String destFile) {

		double distance = 0, totalDistance = 0, unPreDistance = 0;
		float totalDistanceInKm = 0;
		int onDrivingSessionFlag = 0, offDrivingSessionFlag = 0, 
				onDrivingSessionCount = 0, offDrivingSessionCount = 0,sessionCount=0,
				onDrivingStarte = 0, offDrivingStarted = 0;
		long onDrivingStartTime = 0, offDirvingStartTime = 0, startTime = 0, 
				endTime = 0, onEndTime = 0, offEndTime = 0,preTrasition=0,newTrasition=0;
		File inputFile = new File(srcfile);
		File outputFile = new File(destFile);
		ArrayList<DrivingSession> list = new ArrayList<>();

		String line = "";

		int newSpeed = 0, readCount = 0;

		String[] tempArr;
		try {
			FileReader fr = new FileReader(inputFile);
			BufferedReader br = new BufferedReader(fr);
			FileWriter writer = new FileWriter(outputFile);
			while ((line = br.readLine()) != null) {
				readCount++;

				tempArr = line.split(",");
				preTrasition=newTrasition;
				newSpeed = Integer.parseInt(tempArr[1]);
				newTrasition=Long.parseLong(tempArr[0]);
				if (readCount == 1)
				{
					startTime = Long.parseLong(tempArr[0]);
					onDrivingSessionFlag=0;
					offDrivingSessionFlag=0;
				}
				//checking time jumping condition in input file
				
					//processing input file

					if (newSpeed <= Constants.x2) {   //condition A
						offDrivingStarted++;
						endTime = endTime + 1;
						distance = distance + (Double.parseDouble(tempArr[1]) * 2.8);
						//checking session change
						if (offDrivingStarted == Constants.y2) {
							offDirvingStartTime = Long.parseLong(tempArr[0]) - 4;
							if (onDrivingStartTime == 0)
								onDrivingStartTime = startTime;
							sessionCount++;
							onEndTime = offDirvingStartTime - 1;
							offDrivingSessionCount = Constants.y2;
							onDrivingStarte = 0;
							onDrivingSessionCount = 0;
							offDrivingSessionFlag = 1;
							onDrivingSessionFlag = 0;
							totalDistance = distance;
							totalDistanceInKm = (float) totalDistance / 1000;
							DrivingSession drivingSession = new DrivingSession();
							drivingSession.setStartTime(onDrivingStartTime);
							drivingSession.setEndTime(onEndTime);
							drivingSession.setSessionType("on");
							drivingSession.setTotaDistance(totalDistanceInKm);
							if(sessionCount>1)
							{
								// for ignore first session
							list.add(drivingSession);
							System.out.println("on Driving session started At:: " + " " + onDrivingStartTime + " "
									+ "on Driving session ended At:: " + onEndTime + " " + totalDistance + " m" + " "
									+ totalDistanceInKm + " km");
							System.out.println("off Driving session started At:: " + " " + offDirvingStartTime);
							System.out.println("off Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition A::2");
							distance = 0;
							totalDistance = 0;
							totalDistanceInKm = 0;
							}

						} else if (offDrivingStarted < Constants.y2) {
							offDrivingSessionCount++;
							onDrivingStarte = 0;
							onDrivingSessionCount = 0;
							unPreDistance = 0;
							System.out.println(
									"unpredicteble(off) Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
											+ tempArr[3] + " " + tempArr[2] + " Condition A::3" + " " + offDrivingStarted);
						} else if (offDrivingSessionCount > 5 && onDrivingSessionFlag == 1) {
							System.out.println("on Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition A::4");
							onDrivingSessionCount++;
							onDrivingSessionFlag = 1;
							offDrivingSessionFlag = 0;

						}

						else {
							offDrivingSessionCount++;
							offDrivingSessionFlag = 1;
							onDrivingSessionFlag = 0;
							onDrivingStarte = 0;
							System.out.println("off Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition A::6");

						}

					} else if (newSpeed >= Constants.x1) { //condition B (checke on driving trasition)
						distance = distance + (Double.parseDouble(tempArr[1]) * 2.8);
						onDrivingStarte++;
						endTime = endTime + 1;
						if (onDrivingStarte < Constants.y1 && onDrivingSessionFlag == 0) {

							System.out.println("upredicteble(on) Driving Session" + " " + tempArr[0] + " " + " "
									+ tempArr[1] + " " + tempArr[3] + " " + tempArr[2] + " Condition B::1");

						}
						//checking session change condition B
						if (onDrivingStarte == Constants.y1 && onDrivingSessionFlag == 0) {
							onDrivingStartTime = Long.parseLong(tempArr[0]) - 4;
							if (offDirvingStartTime == 0)
								offDirvingStartTime = startTime;
							offEndTime = onDrivingStartTime - 1;
							onDrivingSessionCount = Constants.y1;
							onDrivingStarte = 0;
							offDrivingSessionCount = 0;
							offDrivingStarted = 0;
							onDrivingSessionFlag = 1;
							offDrivingSessionFlag = 0;
							offDrivingStarted = 0;
							totalDistance = distance;
							totalDistanceInKm = (float) totalDistance / 1000;
							DrivingSession drivingSession = new DrivingSession();
							drivingSession.setStartTime(offDirvingStartTime);
							drivingSession.setEndTime(offEndTime);
							drivingSession.setSessionType("off");
							drivingSession.setTotaDistance(totalDistanceInKm);
							System.out.println("On Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition B::2");
							sessionCount++;
							if(sessionCount>1)
							{
								list.add(drivingSession);
							System.out.println("off Driving session started At:: " + " " + offDirvingStartTime + " "
									+ "off Driving session ended At:: " + offEndTime + " " + totalDistance + " m" + " "
									+ totalDistanceInKm + " km");
							System.out.println("on driving session started At:: " + " " + onDrivingStartTime);
							System.out.println("On Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition B::2");
							distance = 0;
							totalDistance = 0;
							totalDistanceInKm = 0;
							}
						} else if (onDrivingSessionFlag == 1 || onDrivingSessionCount > 5) {
							onDrivingSessionCount++;
							offDrivingSessionFlag = 0;
							onDrivingSessionFlag = 1;
							offDrivingSessionCount = 0;
							offDrivingStarted = 0;
							System.out.println("On Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition B::3");

						}

					} else {
						distance = distance + (Double.parseDouble(tempArr[1]) * 2.8);
						if (onDrivingSessionFlag == 1) {
							onDrivingSessionFlag = 1;
							offDrivingSessionFlag = 0;
							endTime = endTime + 1;
							offDrivingStarted = 0;
							onDrivingSessionCount++;
							System.out.println("On Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition C::1");

						} else if (offDrivingSessionFlag == 1) {
							endTime = endTime + 1;
							onDrivingSessionFlag = 0;
							onDrivingStarte = 0;
							offDrivingSessionCount++;
							System.out.println("off Driving Session" + " " + tempArr[0] + " " + " " + tempArr[1] + " "
									+ tempArr[3] + " " + tempArr[2] + " Condition C::2");

						}

					}
					if(onDrivingSessionFlag==1)
					{
						offDrivingSessionFlag=0;
					}
					if(offDrivingSessionFlag==1)
						onDrivingSessionFlag=0;
					endTime = Long.parseLong(tempArr[0]);

					System.out.println();
					
					
					
					
				

			}

			if (offDrivingSessionFlag == 1)
				
			{
				totalDistance = distance;
				totalDistanceInKm = (float) totalDistance / 1000;
				DrivingSession drivingSession = new DrivingSession();
				drivingSession.setStartTime(offDirvingStartTime);
				drivingSession.setEndTime(endTime);
				drivingSession.setSessionType("off");
				drivingSession.setTotaDistance(totalDistanceInKm);
				list.add(drivingSession);
				System.out.println("off Driving session started At:: " + " " + offDirvingStartTime + " "
						+ "off Driving session ended At:: " + endTime + " " + totalDistance + " m" + " "
						+ totalDistanceInKm + " km"+" o 1");
			}
			if (onDrivingSessionFlag == 1) {
				totalDistance = distance;
				totalDistanceInKm = (float) totalDistance / 1000;
				DrivingSession drivingSession = new DrivingSession();
				drivingSession.setStartTime(onDrivingStartTime);
				drivingSession.setEndTime(endTime);
				drivingSession.setSessionType("on");
				drivingSession.setTotaDistance(totalDistanceInKm);
				list.add(drivingSession);
				System.out.println("on Driving session started At:: " + " " + onDrivingStartTime + " "
						+ "on Driving session ended At:: " + endTime + " " + totalDistance + " m" + " "
						+ totalDistanceInKm + " km"+" o 2");

			}

			Iterator<DrivingSession> itr = list.iterator();
			while (itr.hasNext()) {
				//writing output  into the file
				DrivingSession drivingSession = (DrivingSession) itr.next();
				System.out.println(drivingSession.toString());

				writer.write(drivingSession.toString());
				writer.append('\n');

			}

			br.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
