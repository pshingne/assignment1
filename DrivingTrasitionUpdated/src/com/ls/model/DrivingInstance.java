package com.ls.model;

public class DrivingInstance {
	private String type;
	private long timeStamp;
	private float speed;
	private int flag;
	private double distance;
	
	public DrivingInstance()
	{
		this.type = "";
		this.timeStamp = 0;
		this.speed = 0;
		this.flag = 0;
		this.distance = 0;
		
	}
	
	public DrivingInstance(String type, long timeStamp, float speed, int flag, double distance) {
	
		this.type = type;
		this.timeStamp = timeStamp;
		this.speed = speed;
		this.flag = flag;
		this.distance = distance;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return this.type+" "+this.timeStamp+" "+this.speed+" "+this.distance;
	}
	
	
	
	
	
	

}
