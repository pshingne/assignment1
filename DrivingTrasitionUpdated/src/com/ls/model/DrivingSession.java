package com.ls.model;

public class DrivingSession {
	long  startTime;
	long endTime;
	float totaDistance;

	String sessionType;
	public DrivingSession(long startTime, long endTime, String sessionType,float totaDistance) {
	
		this.startTime = startTime;
		this.endTime = endTime;
		this.totaDistance=totaDistance;
		
		this.sessionType = sessionType;
	}
	public DrivingSession(){
		this.startTime = 0;
		this.endTime = 0;
		this.totaDistance=0;
		
		this.sessionType = "";
		
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	
	public String getSessionType() {
		return sessionType;
	}
	public void setSessionType(String sessionTime) {
		this.sessionType = sessionTime;
	}
	public long getTotalTime(){
		return this.endTime-this.startTime;
		
	}
	public float getTotaDistance() {
		return totaDistance;
	}
	public void setTotaDistance(float totaDistance) {
		this.totaDistance = totaDistance;
	}
	@Override
	public String toString() {
		return this.getSessionType()+" "+"Driving session"+","+"Start Time: "+this.getStartTime()+
				","+"End Time: "+this.getEndTime()+","+"Total Distance Travel: "+this.getTotaDistance();
	}
	
	
	

}
