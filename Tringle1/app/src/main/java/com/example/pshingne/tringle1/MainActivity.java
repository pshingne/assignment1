package com.example.pshingne.tringle1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText side1;
    EditText side2;
    EditText side3;
    DrawTringle dtringle;
    DrawTringle dt;
    SharedPreferences preferences;
    Button button;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_main);
        dt=(DrawTringle)new DrawTringle(this);



    }

    public void findtype(View view)

    {
        Float s1,s2,s3;
        MyTriangle myTriangle=MyTriangle.getGetMyTriangleInstance();
        int validityFlag=0,boundFlag=0;
        dtringle=new DrawTringle(this);
        side1 = findViewById(R.id.firstSide);
        side2 = findViewById(R.id.secondSide);
        side3 = findViewById(R.id.thredSide);
        StringBuffer str = new StringBuffer("");


        if( side1.getText().toString().equals("") )
        {
            side1.setError("Please enter side");
        }else if( side2.getText().toString().equals("") )
        {
            side2.setError("Please enter side");
        } else  if( side3.getText().toString().equals("") )
        {
            side3.setError("Please enter side");

        }else

    {
        s1 =  Float.parseFloat(side1.getText().toString());
        s2 = Float.parseFloat(side2.getText().toString());
         s3 = Float.parseFloat(side3.getText().toString());



        if (s1.equals(s2) && s2.equals(s3)) {
            str.append("Triangle is equilateral");
        } else if (s1.equals(s2) || s1.equals(s3) || s2.equals(s3)) {
            str.append("Triangle is isosceles");
        } else {

            str.append("Triangle is scalene");
        }
        myTriangle.setS1(s1);
        myTriangle.setS2(s2);
        myTriangle.setS3(s3);
            myTriangle.setType(str.toString());
        Log.e("main (singleton)s1", ""+myTriangle.getS1());
        Log.e("main (singleton)s2", ""+myTriangle.getS2());
        Log.e("main (singleton)s3", ""+myTriangle.getS3());
        validityFlag=this.isValidTriangle(s1, s2, s3);
        boundFlag=this.isOutOfBound(s1, s2, s3);
        if(boundFlag!=0){
            Toast.makeText(this, "This Tringle is Not possible to Draw ", Toast.LENGTH_LONG).show();

        }

       else if(validityFlag==0  )
        {
            Toast.makeText(this, "This is not valid triangle", Toast.LENGTH_LONG).show();
        }else {
            Intent intent = new Intent(this, CanvasActivity.class);
            startActivity(intent);
        }
    }


    }
   public int isValidTriangle(float s1,float s2,float s3)
    {
        float max;
        int flag=0;
        max=s1>s2?s1:s2;
        max=max>s3?max:s3;
        if(s1==max && (s2+s3)>max)
        {
            flag++;
        }else if(s2==max && (s1+s3)>max){
            flag++;
        }else if(s3==max && (s2+s1)>max)
        {
            flag++;

        }
        return flag;
    }
    public  int isOutOfBound(float s1,float s2,float s3)
    {
        int flag=0;
        if(s1>40 || s2>40 ||s3>40 || s1<=0 || s2<=0 ||s3<=0){
            flag++;
        }
        return flag;

    }


}
