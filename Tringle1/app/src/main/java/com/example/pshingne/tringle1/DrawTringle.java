package com.example.pshingne.tringle1;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DrawTringle extends View {
    Paint bpaint;
    Float s1;
    Float s2;
    Float s3;

    SharedPreferences preferences;




    public DrawTringle(Context context) {

        super(context);
            init(null);
    }

    public DrawTringle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DrawTringle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public DrawTringle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }
    private void init(@Nullable AttributeSet set){

        bpaint=new Paint();



    }
    public  void drawNewTringle(){

        invalidate();
        postInvalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {
        MyTriangle myTriangle=MyTriangle.getGetMyTriangleInstance();


        Paint rr=new Paint();
        Paint bb=new Paint();
        Paint gg=new Paint();
        rr.setColor(Color.RED);
        bb.setColor(Color.BLACK);
        gg.setColor(Color.GREEN);
        bb.setFakeBoldText(true);
        bb.setTextSize(25);
        float x=250;
        float y=450;

        PointF p1=new PointF();
        PointF p2=new PointF();
        PointF p3=new PointF();
        s1= myTriangle.getS1();
        s2=  myTriangle.getS2();
        s3=myTriangle.getS3();
        double tp=(s2*s2)-((s1/2)*(s1/2));
        float topPoint=(float) Math.sqrt(tp);

        Log.e("canvas (singleton)s1", ""+myTriangle.getS1());
        Log.e("canvas (singleton)s2", ""+myTriangle.getS2());
        Log.e("canvas (singleton)s3", ""+myTriangle.getS3());


        Path Dtringle=new Path();


        //Intent intent=Intent.get

        Paint bpaint=new Paint();




        canvas.drawColor(Color.WHITE);
        // tringle=new Path();


        if (s1.equals(s2) && s2.equals(s3))
        {
            bpaint.setColor(Color.RED);
            bpaint.setStyle(Paint.Style.FILL_AND_STROKE);
            bpaint.setAntiAlias(true);
            p1.set(x, y);
            p2.set(s2*10+x,y );
            p3.set((p1.x+p2.x)/2,y-(topPoint*10) );
            canvas.drawText("p1", p1.x-30, p1.y, bb);
            canvas.drawText("p2", p2.x+20, p2.y, bb);
            canvas.drawText("p3", p3.x, p3.y-20, bb);
            Path path = new Path();
            path.moveTo(p1.x, p1.y);
            path.lineTo(p2.x, p2.y);
          //  path.moveTo(p2.x, p2.y);
            path.lineTo(p3.x, p3.y);
          //
             path.moveTo(p3.x, p3.y);
            path.lineTo(p1.x, p1.y);
           // path.close();



           canvas.drawPath(path,bpaint);


        }else if(s1.equals(s2) || s1.equals(s3) || s2.equals(s3)){
            bpaint.setColor(Color.BLUE);

            bpaint.setStyle(Paint.Style.FILL_AND_STROKE);
            bpaint.setAntiAlias(true);
            if(s1.equals(s2))
            {
                p1.set(x, y);
                p2.set(s3*10+x,y );
                p3.set((p1.x+p2.x)/2,y-(topPoint*10) );
            }else if(s2.equals(s3)){
                p1.set(x, y);
                p2.set(s1*10+x,y );
                p3.set((p1.x+p2.x)/2,y-(topPoint*10) );


            }else if(s1.equals(s3)){
                p1.set(x, y);
                p2.set(s2*10+x,y );
                p3.set((p1.x+p2.x)/2,y-(topPoint*10) );


            }

//            canvas.drawCircle(p1.x, p1.y, 10, bpaint);
//            canvas.drawCircle(p2.x, p2.y, 10, bpaint);
//            canvas.drawCircle(p3.x, p3.y, 10, bpaint);
            canvas.drawText("p1", p1.x-30, p1.y, bb);
            canvas.drawText("p2", p2.x+20, p2.y, bb);
            canvas.drawText("p3", p3.x, p3.y-20, bb);

            Path path = new Path();
            path.moveTo(p1.x, p1.y);
            path.lineTo(p2.x, p2.y);
            //  path.moveTo(p2.x, p2.y);
            path.lineTo(p3.x, p3.y);
            //
            path.moveTo(p3.x, p3.y);
            path.lineTo(p1.x, p1.y);
            // path.close();



            canvas.drawPath(path,bpaint);

        }else {
            float diff;
            if(s2>s3)
            {
                diff=s2-s3;
                p1.set(x, y);
                p2.set(s1*10+x,y );
                p3.set((s2*10+(x-diff)),y-(s3*10)+diff );

            }else if(s3>s2){
                diff=s3-s2;
                p1.set(x, y);
                p2.set(s2*10+x,y );
                p3.set((s2*10+x)+diff,y-(s3*10)+diff );
            }

            bpaint.setColor(Color.GREEN);
            bpaint.setStyle(Paint.Style.FILL_AND_STROKE);
            bpaint.setAntiAlias(true);
            canvas.drawText("p1", p1.x-30, p1.y, bb);
            canvas.drawText("p2", p2.x+20, p2.y, bb);
            canvas.drawText("p3", p3.x, p3.y-20, bb);


            Path path = new Path();
            path.moveTo(p1.x, p1.y);
            path.lineTo(p2.x, p2.y);
            //  path.moveTo(p2.x, p2.y);
            path.lineTo(p3.x, p3.y);
            //
            path.moveTo(p3.x, p3.y);
            path.lineTo(p1.x, p1.y);
            // path.close();


            canvas.drawPath(path,bpaint);
        }



    }


}
