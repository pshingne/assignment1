package com.example.pshingne.tringle1;

public class MyTriangle {
    public static MyTriangle getMyTriangleInstance=null;
    private float s1;
    private float s2;
    private  float s3;
    private String type;

    private MyTriangle(float s1, float s2, float s3,String type) {
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.type=type;
    }
    private MyTriangle() {
        this.s1 = 0;
        this.s2 = 0;
        this.s3 = 0;
        this.type="";
    }

    public static MyTriangle getGetMyTriangleInstance() {
        if(getMyTriangleInstance==null)
        {
            getMyTriangleInstance=new MyTriangle();
        }
        return getMyTriangleInstance;
    }

    public float getS1() {
        return s1;
    }

    public float getS2() {
        return s2;
    }

    public float getS3() {
        return s3;
    }

    public String getType() {
        return type;
    }

    public void setS1(float s1) {
        this.s1 = s1;
    }

    public void setS2(float s2) {
        this.s2 = s2;
    }

    public void setS3(float s3) {
        this.s3 = s3;
    }

    public void setType(String type) {
        this.type = type;
    }
}
