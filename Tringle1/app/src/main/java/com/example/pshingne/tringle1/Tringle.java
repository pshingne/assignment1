package com.example.pshingne.tringle1;

public class Tringle{
    private float Side1;
    private float Side2;
    private float side3;
    private StringBuffer type;


    public Tringle(float side1, float side2, float side3) {
       this.Side1 = side1;
        this.Side2 = side2;
        this.side3 = side3;

    }


    public Tringle(float side1, float side2, float side3, StringBuffer type) {
        Side1 = side1;
        Side2 = side2;
        this.side3 = side3;
        this.type = type;
    }

    public StringBuffer getType() {
        return type;
    }

    public void setType(StringBuffer type) {
        this.type = type;
    }

    public Tringle() {
        this.Side1 = 0;
       this.Side2 = 0;
        this.side3 = 0;
    }

    public float getSide1() {
        return this.Side1;
    }

    public void setSide1(float side1) {
        this.Side1 = side1;
    }

    public float getSide2() {
        return this.Side2;
    }

    public void setSide2(float side2) {
       this.Side2 = side2;
    }

    public float getSide3() {
        return this.side3;
    }

    public void setSide3(float side3) {
        this.side3 = side3;
    }

}
