package com.example.pshingne.tringle1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CanvasActivity extends AppCompatActivity{
   TextView myTringleType;
   ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyTriangle myTriangle=MyTriangle.getGetMyTriangleInstance();

        Float s1= myTriangle.getS1();
        Float s2= myTriangle.getS2();
        Float s3=myTriangle.getS3();
        String type=myTriangle.getType();
        setContentView(R.layout.activity_canvas);
        myTringleType=findViewById(R.id.t_type);
        myTringleType.setText(type);
        imageView=findViewById(R.id.triangleImage);
        if(type.equals("Triangle is equilateral")){
            imageView.setImageResource(R.drawable.equilat);

        }else if(type.equals("Triangle is isosceles")){
            imageView.setImageResource(R.drawable.iso);


        }else {
            imageView.setImageResource(R.drawable.scale);


        }
    }
}
